# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from pythonforandroid.recipe import PythonRecipe


class TrytonRPC(PythonRecipe):

    version = '6.0'
    url = ('https://gitlab.com/datalifeit/tryton_rpc/-/'
        'archive/{version}/tryton_rpc-{version}.zip' % {
            'version': version})
    depends = ['python3', 'setuptools']
    site_packages_name = 'tryton_rpc'
    call_hostpython_via_targetpython = False


recipe = TrytonRPC()
