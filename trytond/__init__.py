# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from pythonforandroid.recipe import PythonRecipe
from pythonforandroid.toolchain import current_directory


class TrytondRecipe(PythonRecipe):

    version = '6.0'
    url = 'https://files.pythonhosted.org/packages/9e/45/1a03e8ca58258e2351c941f6834fce0cfb45d92798df9c541c6838e99c33/trytond-6.0.38.tar.gz'
    depends = ['python3', 'setuptools', 'sqlite3', 'lxml']
    python_depends = [
        'werkzeug',
        'python-sql',
        'python-dateutil',
        'wrapt',
        'polib',
        'Genshi',
        'relatorio[fodt] >= 0.7.0',
        'python-stdnum',
        'passlib >= 1.7.0',
        'defusedxml',
        'lxml >= 2.0',
        'simpleeval'
    ]
    site_packages_name = 'trytond'
    call_hostpython_via_targetpython = False
    patches = ['trytond.patch']


recipe = TrytondRecipe()
