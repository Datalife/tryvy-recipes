from pythonforandroid.toolchain import (PythonRecipe, shutil,
    current_directory)
from os.path import exists, join


class ZbarQrCodeRecipe(PythonRecipe):
    version = '0.1'
    url = ('https://gitlab.com/datalifeit/kivy-zbar_qrcode' +
        '/get/default.zip')
    depends = [('python2'), 'setuptools', 'kivy', 'android']
    site_packages_name = 'zbar_qrcode'
    call_hostpython_via_targetpython = False

    def should_build(self, arch):
        return (super(ZbarQrCodeRecipe, self).should_build(arch) or
            not exists(join(self.ctx.get_libs_dir(arch.arch), 'libiconv.so')))

    def build_arch(self, arch):
        super(ZbarQrCodeRecipe, self).build_arch(arch)
        with current_directory(self.get_build_dir(arch.arch)):
            for _file in ('libiconv.so', 'libzbarjni.so'):
                shutil.copyfile('libs/android/%s' % _file,
                    join(self.ctx.get_libs_dir(arch.arch), _file))


recipe = ZbarQrCodeRecipe()
