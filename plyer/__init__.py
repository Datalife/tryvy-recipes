# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from pythonforandroid.recipe import PythonRecipe


class TrytondRecipe(PythonRecipe):

    version = '2.1.0'
    url = 'http://github.com/kivy/plyer/releases/download/{version}/plyer-{version}.tar.gz'
    depends = []
    site_packages_name = 'plyer'
    call_hostpython_via_targetpython = False
    patches = ['notification.patch']


recipe = TrytondRecipe()
